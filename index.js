const morgan = require('morgan');
const bodyParser = require('body-parser');
const express = require('express');
const jwt = require('jsonwebtoken');
const app = express();

const apiRoute = require('./routes/api');



//prueba-.
app.get('/api', (req, res)=>{
    res.json({
        message: 'welcome'
    });
});


app.post('/api/posts', (req,res)=>{
    res.json({
        message: 'post'
    });
});



//configuración
app.set('json spaces', 4);

//middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//routes
app.use('/api',apiRoute)

//static files

//start server
app.listen(3000, ()=>{
    console.log('server on port', 3000);
})