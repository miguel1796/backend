const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();

router.route('/login')
    .get((req, res)=>{
        res.json({usuario: 'prueba',
                contraseña: 'prueba'})
    })
    
    .post(verifyToken, (req,res)=>{
        jwt.verify(req.token, 'secretkey',(err,authData)=>{
            if(err){
                res.sendStatus(403);

            }else{
                res.json({
                    message: 'pst gcreado',
                    authData
                });
            }
        });

        //mock user
        const user = {
            id:1,
            username: 'brad',
            email : 'df'
        }

        jwt.sign(
            {user},'secrekey', {exouresIn:'30s'},(err,token)=>{   
            res.json({
                token:token
            })
        });
    });


//formato de los tokens

//authorization : bearer <access_token>


function verifyToken(req,res,next){
    //get the auth header value
    const bearerHeader = req.headers['authorization'];

    //check si el bearer es indefinido
    if(typeof bearerHeader !== 'undefined'){
        //split en espacios
        const bearer = bearerHeader.split(' ');
        //obtener token de un array
        const bearerToken = bearer[1];

        //setear ese token
        req.token = bearerToken;

        //siguiente middleware
        next();
    }else{
        //prohibido..
        res.sendStatus(403);
    }
}

module.exports = router;